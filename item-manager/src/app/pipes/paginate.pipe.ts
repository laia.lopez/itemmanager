import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paginate'
})
export class PaginatePipe implements PipeTransform {

  transform(items: any[], pageSize: number | string, pageNumber: number) {
    // Si no hay ítems devolvemos el array vacío.
    if (!items.length) return [];
    // Si queremos que aparezcan todos los registros...
    if (pageSize === 'all') {
      return items;
    }

    pageSize = pageSize || 5;
    pageNumber = pageNumber || 1;
    // Para permitir el índice 0...
    --pageNumber;

    // Retornamos dinámicamente los elementos correspondientes... 
    return items.slice(pageNumber * +pageSize, (pageNumber + 1) * +pageSize);
  }

}
