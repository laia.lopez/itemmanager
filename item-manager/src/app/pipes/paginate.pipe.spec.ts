import { PaginatePipe } from './paginate.pipe';

describe('PaginatePipe', () => {
  const pipe = new PaginatePipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('if not items should return empty array', () => {
    const emptyArray = pipe.transform([], 2, 4);
    expect(emptyArray).toEqual([]);
  });

  it('if have items and PageSize = all should return all items', () => {
    const items = ['a', 'b'];
    const fullArray = pipe.transform(items, 'all', 4);
    expect(fullArray).toEqual(items);
  });

  it('if have items and PageSize = null it will set 5 should return five items', () => {
    const items = ['a', 'b', 'c', 'd', 'e', 'f'];
    const arrayFive = pipe.transform(items, null, null);
    expect(arrayFive.length).toEqual(5);
  });
});
