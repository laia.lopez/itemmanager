

import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { TranslateService, TranslateStore, TranslateLoader, TranslateCompiler, USE_DEFAULT_LANG, USE_STORE, USE_EXTEND, DEFAULT_LANGUAGE, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { RouterModule, ChildrenOutletContexts } from '@angular/router';
import { HeaderComponent } from './components/general/header/header.component';
import { APP_BASE_HREF } from '@angular/common';
import { HttpLoaderFactory } from './app.module';
import { GlobalService } from './services/global/global.service';
import { Renderer2, Type } from '@angular/core';
import { throwError, Subscription } from 'rxjs';
import { FooterComponent } from './components/general/footer/footer.component';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  let translate: TranslateService;
  let globalSv: GlobalService;
  let renderer: Renderer2;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent
      ],
      imports: [
        RouterModule.forRoot([{ path: "", component: AppComponent}]),
        TranslateModule.forRoot(
          {
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            }
          }
        ),
        HttpClientModule
      ],
      providers: [
        { provide: USE_DEFAULT_LANG, useValue: 'en' },
        { provide: DEFAULT_LANGUAGE, useValue: 'en' },
        { provide: USE_STORE, useValue: {} },
        { provide: USE_EXTEND, useValue: {} },
        { provide: APP_BASE_HREF, useValue : '/' },
        TranslateService,
        TranslateStore,
        TranslateCompiler,
        ChildrenOutletContexts,
        Renderer2
      ]
    }).compileComponents();
    translate = TestBed.get(TranslateService);
    globalSv = TestBed.get(GlobalService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    renderer = fixture.componentRef.injector.get<Renderer2>(Renderer2 as Type<Renderer2>);
  });

  it('Debe crearse AppComponent', (() => {
    expect(component).toBeTruthy();
  }));

  
  it('ngOnInit() debe llamar a subscribeToModalObservable()', () => {
    spyOn(component, 'subscribeToModalObservable');
    
    component.ngOnInit();
    expect(component.subscribeToModalObservable).toHaveBeenCalled();
});

  it('unsubscribes when destoryed', () => {
    component.subscription = new Subscription();
    spyOn(component.subscription, 'unsubscribe');

    component.ngOnDestroy();
    expect(component.subscription.unsubscribe).toHaveBeenCalledTimes(1);
  });

  // Si currentIsModal$ devuelve true...
  it('addClass()', () => {
    globalSv.changeModalOpened(true);
    
    spyOn(renderer, 'addClass');
    component.subscribeToModalObservable();
    fixture.detectChanges();

    expect(renderer.addClass).toHaveBeenCalled();
    });

  // Si currentIsModal$ devuelve false...
  it('removeClass()', () => {
    globalSv.changeModalOpened(false);
    
    spyOn(renderer, 'removeClass');
    component.subscribeToModalObservable();
    fixture.detectChanges();

    expect(renderer.removeClass).toHaveBeenCalled();
    });

  // Si currentIsModal$ devuelve error...
  it('subscribeToModalObservable() error debe llamar a alert', () => {
    globalSv.currentIsModal$ = throwError(new Error('Fake error'));

    spyOn(window, 'alert');
    component.subscribeToModalObservable();
    
    expect(window.alert).toHaveBeenCalled();
  });
});
