import { Component, OnInit, Renderer2, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { TranslateService } from '../../node_modules/@ngx-translate/core';
import { GlobalService } from './services/global/global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  subscription: Subscription;

  constructor(private translate: TranslateService, private globalService: GlobalService, private renderer: Renderer2) {
    this.translate.addLangs(['es', 'en']);
    this.translate.setDefaultLang('en');
  }

  ngOnInit(): void {
    this.subscribeToModalObservable();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  subscribeToModalObservable(): void {
    this.subscription = this.globalService.currentIsModal$.subscribe(
                          res => {
                            if (res) {
                              this.renderer.addClass(document.body, 'modalIsActive');
                            } else {
                              this.renderer.removeClass(document.body, 'modalIsActive');
                            }
                          },
                          err => alert(this.translate.instant('err.500'))
                        )
  }
}
