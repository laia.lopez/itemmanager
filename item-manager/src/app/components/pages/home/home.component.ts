import _ from 'lodash';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ItemI } from '../../../models/item-interface';
import { ItemTranslatedI } from '../../../models/item-translated.interface';

import { PageEvent, MatPaginator } from '@angular/material/paginator';

import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { GlobalService } from 'src/app/services/global/global.service';
import { DataService } from '../../../services/data/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [DataService]
})

export class HomeComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  filteredItems: ItemI[] = [];
  items: ItemI[] = []; 
  // Permitiremos filtrar por: title, description, price o email.
  keysToOrder: string[] = ['title', 'description', 'price', 'email'];
  translatedItemsPerPage: ItemTranslatedI[] = [];

  openItemsModal = false;
  pageNumber = 1;
  pageSize = 5;

  // Permite paginar dinámicamente: en el caso del ejemplo eran cinco pero también permitimos paginar por diez.
  pageSizeOptions = [5];
  subscriptions: Subscription[] = [];

  constructor(private dataSv: DataService, private translate: TranslateService, private globalService: GlobalService) {
    this.listenCurrentItems();
  }

  ngOnInit(): void {
    this.listenWhenTranslateChange();
    this.getItems();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  ngAfterViewInit() : void {
    ['en', 'es'].forEach(
      (lang: string) => {
        this.subscriptions.push(
          this.translate.getTranslation(lang).subscribe(translations => {            
            this.translatedItemsPerPage.push({
              lang,
              translation: translations.home.itemsPerPage
            });
          })
        );
      });
  }

  listenWhenTranslateChange(): void {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.paginator._intl.itemsPerPageLabel = this.translatedItemsPerPage.find(item => item.lang === event.lang).translation;
    });  
  }

  getItems(): void {
    // Petición a nuestro servicio para obtener ítems.
    this.dataSv.getItems().then(
      result => {
        this.globalService.updateItems(result.items);
      },
      err => {
        alert(this.translate.instant('err.500'));
      }
    )
  }

  listenCurrentItems(): void {
    this.subscriptions.push(
      this.globalService.currentItems$.subscribe(
        result => {
          this.items = result.items;
          if (result.doRefresh) {
            this.filteredItems = _.cloneDeep(this.items);
          }
        }, err => {
          alert(this.translate.instant('err.500'));
        }
      )
    );
  }

  handlePage(e: PageEvent) {
    this.pageSize = e.pageSize;

    // Para contrarestar el índice del array...
    this.pageNumber = e.pageIndex + 1;
  }

  clickHeart(item: ItemI): void {
    // Toggle favourite en el array filteredItems.
    item.favourite = item.favourite ? !item.favourite : true;

    const itemWithoutHeart = _.cloneDeep(item);
    delete itemWithoutHeart.favourite;

    // Por si tenemos seteado el ítem pero guardado con el valor favorito distinto al actual.
    const reverseItem = _.cloneDeep(item);
    reverseItem.favourite = !reverseItem.favourite;

    // Guardamos favourite en el array inicial para no perderlo a la hora de filtrar o abrir el modal.
    const initialItems = this.items;
    const findedInitialItem = initialItems.findIndex(itemF => 
      JSON.stringify(itemF) === JSON.stringify(itemWithoutHeart) ||
      JSON.stringify(itemF) === JSON.stringify(item) ||
      JSON.stringify(itemF) === JSON.stringify(reverseItem));

    if (findedInitialItem !== -1) {
      initialItems[findedInitialItem].favourite = item.favourite;
    }
    this.globalService.updateItems(initialItems, false);
  }

  getMailTo(itemMail: string): string {
    return 'mailto:' + itemMail;
  }

  updateFilteredItems($event: ItemI[], calledFromSB = false): void {
    this.filteredItems = _.cloneDeep($event);
    this.paginator.firstPage();
    // Deseleccionamos el valor actual del filtrado "order by"
    if (calledFromSB){
      this.globalService.updateResetSelector(true);
    }
  }

  openModal(): void {
    this.openItemsModal = true;
    // Esto terminará afectando al NgClass del AppComponent y se ocultará la barra de scroll.
    this.globalService.changeModalOpened(true);
  }

  closeModal(): void {
    this.openItemsModal = false;
    this.globalService.changeModalOpened(false);
  }
}
