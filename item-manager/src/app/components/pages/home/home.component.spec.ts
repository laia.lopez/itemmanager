import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { TranslateService, TranslateStore, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpLoaderFactory } from 'src/app/app.module';
import { Subscription, throwError, Observable, of } from 'rxjs';
import { GlobalService } from 'src/app/services/global/global.service';
import { MatPaginatorModule, MatPaginator } from '@angular/material/paginator';
import { SortbyComponent } from '../../dynamic/sortby/sortby.component';
import { SearchbyComponent } from '../../dynamic/searchby/searchby.component';
import { FormsModule } from '@angular/forms';
import { ItemI } from 'src/app/models/item-interface';
import { ChangeDetectorRef } from '@angular/core';
import { PaginatePipe } from 'src/app/pipes/paginate.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataService } from 'src/app/services/data/data.service';

describe('HomeComponent', () => {
  const items: ItemI[] = [{ title: 'A', description: 'A', price: '2', email: 'A', image: 'A'}];

  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let translate: TranslateService;
  let globalService: GlobalService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent, SortbyComponent, SearchbyComponent, PaginatePipe ],
      imports: [
        TranslateModule.forRoot(
          {
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            }
          }
        ),
        HttpClientModule,
        MatPaginatorModule,
        BrowserAnimationsModule,
        FormsModule
      ],
      providers: [
        TranslateService,
        DataService,
        TranslateStore,
        MatPaginator,
        ChangeDetectorRef
      ]
    })
    .compileComponents();
    translate = TestBed.get(TranslateService);
    globalService = TestBed.get(GlobalService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    component.subscriptions = [];
    component.translatedItemsPerPage = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit() debe llamar a getItems()', () => {
    globalService.updateItems(items);
    spyOn(component, 'getItems');
    spyOn(globalService, 'updateItems').and.callThrough();
    
    component.ngOnInit();
    fixture.detectChanges();

    expect(component.getItems).toHaveBeenCalled();
  });

  it('forEach called when ngOnDestroy()', () => {
    spyOn(component.subscriptions, 'forEach');

    component.ngOnDestroy();

    expect(component.subscriptions.forEach).toHaveBeenCalledTimes(1);
  });

  it('listenWhenTranslateChange() seteará component.paginator._intl.itemsPerPageLabel', () => {
    component.translatedItemsPerPage = [{lang:'en', translation:'Items per page:'}];
    translate.use('en');

    expect(component.paginator._intl.itemsPerPageLabel).toEqual(component.translatedItemsPerPage.find(item => item.lang === 'en').translation);
  });

  it('listenWhenTranslateChange() shouldnt do find', () => {
    spyOn(component.translatedItemsPerPage, 'find');
    
    // Si llamamos a la función directamente no tendrá valor la subscripción del Translate.
    component.listenWhenTranslateChange();
    expect(component.translatedItemsPerPage.find).not.toHaveBeenCalled();
  });

  it('listenCurrentItems() error debe llamar a alert', () => {
    spyOn(window, 'alert');
    globalService.currentItems$ = throwError(new Error('Fake error'));
    component.listenCurrentItems();
    expect(window.alert).toHaveBeenCalled();
  });

  it('handlePage() should equal pageSize and change pageNumber', () => {
    component.handlePage(component.paginator);
    fixture.detectChanges();

    expect(component.pageSize).toEqual(component.paginator.pageSize);
    expect(component.pageNumber ).toEqual(component.paginator.pageIndex +1);
  });

  it('getMailTo() should equal mailto + string', () => {
    const mailtoC = 'mailto:', mailC = 'laialopezz99@gmail.com'
    const returnedMail = component.getMailTo('laialopezz99@gmail.com');

    expect(returnedMail).toEqual(mailtoC + mailC);
  });

  it('openModal() set openItemsModal to true + call globalService.changeModalOpened', () => {
    spyOn(globalService, 'changeModalOpened');
    component.openModal();

    expect(component.openItemsModal).toBeTruthy();
    expect(globalService.changeModalOpened).toHaveBeenCalled();
  });

  it('closeModal() set openItemsModal to false + call globalService.changeModalOpened', () => {
    spyOn(globalService, 'changeModalOpened');
    component.closeModal();

    expect(component.openItemsModal).toBeFalsy();
    expect(globalService.changeModalOpened).toHaveBeenCalled();
  });

  it('updateFilteredItems() should update filteredITems + call paginator', () => {
    spyOn(component.paginator, 'firstPage');
    spyOn(globalService, 'updateResetSelector').and.callThrough();

    component.updateFilteredItems(items);
    fixture.detectChanges();

    expect(component.filteredItems).toEqual(items);
    expect(component.paginator.firstPage).toHaveBeenCalled();
    expect(globalService.updateResetSelector).not.toHaveBeenCalled();
  });

  it('updateFilteredItems() should update filteredITems + call paginator and globalService', () => {
    spyOn(component.paginator, 'firstPage');
    spyOn(globalService, 'updateResetSelector').and.callThrough();

    component.updateFilteredItems(items, true);
    fixture.detectChanges();

    expect(component.filteredItems).toEqual(items);
    expect(component.paginator.firstPage).toHaveBeenCalled();
    expect(globalService.updateResetSelector).toHaveBeenCalled();
  });

  it('clickHeart() toggle favourite and globalService.updateITems', () => {
    const itemHeart: ItemI = { title: 'Pienso', description: 'A', price: '2', email: 'laialopezz99@gmail.com', image: 'A', favourite: true};
    const itemWithoutHeart: ItemI = { title: 'Pienso 2', description: 'A', price: '2', email: 'laialopezz99@gmail.com', image: 'A', favourite: false};
    const itemNotInItems: ItemI = { title: 'Pienso 3', description: 'A', price: '2', email: 'laialopezz99@gmail.com', image: 'A', favourite: false};

    globalService.updateItems([itemHeart, itemWithoutHeart])
    spyOn(globalService, 'updateItems').and.callThrough();

    component.clickHeart(itemHeart);
    component.clickHeart(itemWithoutHeart);
    component.clickHeart(itemNotInItems);

    fixture.detectChanges();

    expect(globalService.updateItems).toHaveBeenCalled();
  });
  
});
