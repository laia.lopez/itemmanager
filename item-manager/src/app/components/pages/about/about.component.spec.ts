import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AboutComponent } from './about.component';
import { HttpLoaderFactory } from 'src/app/app.module';
import { OwnTranslateService } from 'src/app/services/own-translate/own-translate.service';

describe('AboutComponent', () => {
  let component: AboutComponent;
  let fixture: ComponentFixture<AboutComponent>;
  let ownTranslateService: OwnTranslateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutComponent ],
      imports: [
        TranslateModule.forRoot(
          {
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            }
          }
        ),
        HttpClientModule
      ]
    })
    .compileComponents();
    ownTranslateService = TestBed.get(OwnTranslateService)

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('translate() should call ownTranslateService.getTranslate()', () => {
    spyOn(ownTranslateService, 'getTranslate');
    
    component.translate('home', 'title');
    expect(ownTranslateService.getTranslate).toHaveBeenCalled();  
  });

  it('goToSpotify() should open window', () => {
    spyOn(window, 'open');
    
    component.goToSpotify();
    expect(window.open).toHaveBeenCalled();  
  });
});
