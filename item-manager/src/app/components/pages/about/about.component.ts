import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { OwnTranslateService } from 'src/app/services/own-translate/own-translate.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  valuesToShow: string[] = ['name', 'hobbies', 'favOS', 'favLang', 'hogwartsHouse', 'favSong'];

  constructor(public translateSv: TranslateService, private ownTranslateService: OwnTranslateService) { }

  ngOnInit(): void {
  }

  translate(literalString: string, key: string): string {
    return this.ownTranslateService.getTranslate(literalString, key);
  }

  goToSpotify(): void {
    window.open("https://open.spotify.com/track/6flhPfJHRcQ1jiXojPen9J?si=TE1SS1AwTP2UIygpdOFnUQ", "_blank");
  }

}
