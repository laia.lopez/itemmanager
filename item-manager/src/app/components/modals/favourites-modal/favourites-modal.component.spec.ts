import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FavouritesModalComponent } from './favourites-modal.component';
import { HttpLoaderFactory } from 'src/app/app.module';
import { ItemI } from 'src/app/models/item-interface';
import { GlobalService } from 'src/app/services/global/global.service';
import { SearchbyComponent } from '../../dynamic/searchby/searchby.component';
import { FormsModule } from '@angular/forms';
import { throwError } from 'rxjs';

describe('FavouritesModalComponent', () => {
  const itemHeart: ItemI = { title: 'Pienso', description: 'A', price: '2', email: 'laialopezz99@gmail.com', image: 'A', favourite: true};
  const itemNotInItems: ItemI = { title: 'Pienso 3', description: 'A', price: '2', email: 'laialopezz99@gmail.com', image: 'A', favourite: false};
  
  let component: FavouritesModalComponent;
  let fixture: ComponentFixture<FavouritesModalComponent>;
  let globalService: GlobalService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavouritesModalComponent, SearchbyComponent],
      imports: [
        TranslateModule.forRoot(
          {
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            }
          }
        ),
        HttpClientModule,
        FormsModule
      ]
    })
    .compileComponents();
    globalService = TestBed.get(GlobalService);

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouritesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('clickHeart() toggle favourite and globalService.updateITems', () => {
    globalService.updateItems([itemHeart])
    spyOn(globalService, 'updateItems').and.callThrough();

    component.clickHeart(itemHeart);
    component.clickHeart(itemNotInItems);
    fixture.detectChanges();

    expect(globalService.updateItems).toHaveBeenCalled();
  });

  it('call updateFilteredItems(:parameter) will equal favouriteItemsF to parameter', () => {
    component.updateFilteredItems([itemHeart]);
    fixture.detectChanges();

    expect(component.favouriteItemsF).toEqual([itemHeart]);
  });

  it('listenCurrentItems() error debe llamar a alert', () => {
    spyOn(window, 'alert');
    globalService.currentItems$ = throwError(new Error('Fake error'));
    component.listenCurrentItems();
    expect(window.alert).toHaveBeenCalled();
  });

});
