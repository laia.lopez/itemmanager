import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ItemI } from 'src/app/models/item-interface';

import { TranslateService } from '@ngx-translate/core';
import { GlobalService } from 'src/app/services/global/global.service';

import _ from 'lodash';

@Component({
  selector: 'app-favourites-modal',
  templateUrl: './favourites-modal.component.html',
  styleUrls: ['./favourites-modal.component.scss']
})
export class FavouritesModalComponent implements OnInit, OnDestroy {
  normalItems: ItemI[] = [];
  favouriteItems: ItemI[] = [];
  favouriteItemsF: ItemI[] = [];
  ownAtrToSearch = ['title'];
  subscription: Subscription;

  constructor(private translate: TranslateService, private globalService: GlobalService) {
    this.listenCurrentItems();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  listenCurrentItems(): void {
    this.subscription = this.globalService.currentItems$.subscribe(
                          resp => {
                            // Todos los ítems
                            this.normalItems = resp.items;
                            // Favoritos sin filtrado
                            this.favouriteItems = this.normalItems.filter(item => item && item.favourite);
                            // Favoritos con filtrado
                            this.favouriteItemsF = _.cloneDeep(this.favouriteItems);
                          },
                          err => {
                            alert(this.translate.instant('err.500'));
                          }
                        )
  }

  clickHeart(item: ItemI): void {
     // Toggle favourite en el array filteredItems.
     item.favourite = item.favourite ? !item.favourite : true;

     // Por si tenemos seteado el ítem pero guardado con el valor favorito distinto al actual.
     const reverseItem = _.cloneDeep(item);
     reverseItem.favourite = !reverseItem.favourite;
 
     const initialItems = this.normalItems;
     const findedInitialItem = initialItems.findIndex(itemF => 
       JSON.stringify(itemF) === JSON.stringify(item) ||
       JSON.stringify(itemF) === JSON.stringify(reverseItem)
       );
 
     if (findedInitialItem !== -1) {
       initialItems[findedInitialItem].favourite = item.favourite;
       if (!initialItems[findedInitialItem].favourite) {
         alert(this.translate.instant('succed.removeFavItem'));
       }
     }
     this.globalService.updateItems(initialItems);
  }

  updateFilteredItems(newFavouriteItems: ItemI[]): void {
    this.favouriteItemsF = newFavouriteItems;
  }

}
