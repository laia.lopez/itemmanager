import { Component, OnInit } from '@angular/core';
import { TranslateService } from '../../../../../node_modules/@ngx-translate/core';
import { OwnTranslateService } from '../../../services/own-translate/own-translate.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  languages = ['en', 'es'];
  selectedLanguage: string;

  constructor(public translateService: TranslateService, private ownTranslateService: OwnTranslateService, private router: Router) { }

  ngOnInit(): void {
    this.getDefaultLang();
  }

  getDefaultLang(): void {
    this.selectedLanguage = this.translateService.getDefaultLang();
  }

  setNewLanguage(newLanguage: string): void {
    this.selectedLanguage = newLanguage;
    this.translateService.use(newLanguage);
  }

  translate(text: string, value: string): string {
    return this.ownTranslateService.getTranslate(text, value);
  }

  goToHome(): void {
    this.router.navigateByUrl('/home');
  }
}
