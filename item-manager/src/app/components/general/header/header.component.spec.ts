import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateService, TranslateStore, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpLoaderFactory } from 'src/app/app.module';

import { HeaderComponent } from './header.component';
import { RouterModule, Router } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let translate: TranslateService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ],
      imports: [
        RouterModule.forRoot([{ path: "", component: HeaderComponent}]),
        TranslateModule.forRoot(
          {
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            }
          }
        ),
        HttpClientModule
      ],
      providers: [
        TranslateService,
        TranslateStore,
        {provide: APP_BASE_HREF, useValue : '/' }
      ]
    })
    .compileComponents();
    translate = TestBed.get(TranslateService);
    router = TestBed.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('setNewLanguage() should set equal selectedLanguage var', () => {
    component.setNewLanguage('en')
    fixture.detectChanges();
    expect(component.selectedLanguage).toEqual('en');
  });

  it('call setNewLanguage() should call translate.use', () => {
    spyOn(translate, 'use');
    component.setNewLanguage('en');
    expect(translate.use).toHaveBeenCalled();
  });

  it('call goHome() should call router.navigateByUrl', () => {
    spyOn(router, 'navigateByUrl');
    component.goToHome();
    expect(router.navigateByUrl).toHaveBeenCalled();
  });
});
