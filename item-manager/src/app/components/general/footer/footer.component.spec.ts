import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateService, TranslateStore, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpLoaderFactory } from 'src/app/app.module';

import { FooterComponent } from './footer.component';
import { RouterModule, Router } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterComponent ],
      imports: [
        RouterModule.forRoot([{ path: "", component: FooterComponent}]),
        TranslateModule.forRoot(
          {
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            }
          }
        ),
        HttpClientModule
      ],
      providers: [
        TranslateService,
        TranslateStore,
        {provide: APP_BASE_HREF, useValue : '/' }
      ]
    })
    .compileComponents();
    router = TestBed.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('call goToAbout() should call router.navigateByUrl', () => {
    spyOn(router, 'navigateByUrl');
    component.goToAbout();
    expect(router.navigateByUrl).toHaveBeenCalled();
  });
});
