import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchbyComponent } from './searchby.component';
import { TranslateService, TranslateStore, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpLoaderFactory } from 'src/app/app.module';
import { FormsModule } from '@angular/forms';
import { ItemI } from 'src/app/models/item-interface';

describe('SearchbyComponent', () => {
  let component: SearchbyComponent;
  let fixture: ComponentFixture<SearchbyComponent>;
  let translate: TranslateService;
  const items: ItemI[] = [
    {
      title: 'laia',
      description: 'laia',
      price: 'laia',
      email: 'laia',
      image: 'laia.img',
      favourite: true
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchbyComponent ],
      imports: [
        TranslateModule.forRoot(
          {
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            }
          }
        ),
        HttpClientModule,
        FormsModule
      ],
      providers: [
        TranslateService,
        TranslateStore
      ]
    })
    .compileComponents();
    translate = TestBed.get(TranslateService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchbyComponent);
    component = fixture.componentInstance;
    component.initialCleanItems = items;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('emitFilterItems() with no textToSearch should call alert', () => {
    spyOn(window, 'alert');
    component.textToSearch = null;
    component.emitFilterItems();

    fixture.detectChanges();
    expect(window.alert).toHaveBeenCalled();
  });

  it('emitFilterItems() with textToSearch should call apply filter and emit', () => {
    spyOn(component.filteredItemsE, 'emit');
    spyOn(component, 'applyFilter');

    component.textToSearch = 'Laia';
    component.emitFilterItems();
    
    fixture.detectChanges();
    expect(component.applyFilter).toHaveBeenCalled();
    expect(component.filteredItemsE.emit).toHaveBeenCalled();
  });

  it('applyFilter() with atrToSearchBy should call apply searchByAllAtr()', () => {
    component.atrToSearchBy = null;

    spyOn(component, 'searchByAllAtr');
    component.applyFilter('');
    fixture.detectChanges();

    expect(component.searchByAllAtr).toHaveBeenCalled();
  });
  
  it('applyFilter() without atrToSearchBy should call apply searchByOwnAtr()', () => {
    component.atrToSearchBy = ['Laia'];

    spyOn(component, 'searchByOwnAtr');
    component.applyFilter('');
    fixture.detectChanges();

    expect(component.searchByOwnAtr).toHaveBeenCalled();
  });

  // Existe un valor 'Laia' en un objeto del array.
  it('searchByAllAtr() should return one item', () => {
    const itemsSearched = component.searchByAllAtr('Laia');

    fixture.detectChanges();
    expect(itemsSearched.length).toBe(items.length);
  });
  
  // 'Marc' no existe en ningún valor del objeto del array.
  it('searchByAllAtr() should return zero items', () => {
    const itemsSearched = component.searchByAllAtr('Marc');

    fixture.detectChanges();
    expect(itemsSearched.length).toBe(0);
  });

  // Existe un valor 'Laia' en el array y dicho objecto contiene una key 'title'.
  it('searchByOwnAtr() should return one item', () => {
    component.atrToSearchBy = ['title'];
    const itemsSearched = component.searchByOwnAtr('Laia');

    fixture.detectChanges();
    expect(itemsSearched.length).toBe(items.length);
  });


  // Existe un valor 'Laia' en el array pero dicho objecto NO contiene una key 'favouriteColor'.
  // 'Marc' ni existe ni tiene la key 'favouriteColor'.
  it('searchByOwnAtr() should return zero items', () => {
    component.atrToSearchBy = ['favouriteColor'];
    const itemsSearchedLaia = component.searchByOwnAtr('Laia');
    const itemsSearchedMarc = component.searchByOwnAtr('Marc');

    fixture.detectChanges();
    expect(itemsSearchedLaia.length).toBe(0);
    expect(itemsSearchedMarc.length).toBe(0);
  });

  it('searchByOwnAtr() should return zero items', () => {
    component.atrToSearchBy = ['favouriteColor'];
    const itemsSearchedLaia = component.searchByOwnAtr('Laia');
    const itemsSearchedMarc = component.searchByOwnAtr('Marc');

    fixture.detectChanges();
    expect(itemsSearchedLaia.length).toBe(0);
    expect(itemsSearchedMarc.length).toBe(0);
  });

  it('cleanInput() should reset textToSearch and emit with filteredItemsE', () => {
    spyOn(component.filteredItemsE, 'emit');

    component.cleanInput();
    fixture.detectChanges();

    expect(component.textToSearch).toEqual('');
    expect(component.filteredItemsE.emit).toHaveBeenCalled();
  });

  it('filteredItemsE should emit []', () => {
    const emptyArray: ItemI[] = [];
    component.initialCleanItems = null;

    spyOn(component.filteredItemsE, 'emit').and.callThrough();
    component.cleanInput();
    fixture.detectChanges();

    const mostRecentValueE: any = (component.filteredItemsE.emit as any).calls.mostRecent().args[0];

    expect(mostRecentValueE).toEqual(emptyArray);
  });
  
});
