import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ItemI } from '../../../models/item-interface';

@Component({
  selector: 'app-searchby',
  templateUrl: './searchby.component.html',
  styleUrls: ['./searchby.component.scss']
})
export class SearchbyComponent implements OnInit {
  @Output() filteredItemsE: EventEmitter<ItemI[]> = new EventEmitter<ItemI[]>();
  @Input() initialCleanItems: ItemI[] = [];
  @Input() atrToSearchBy: string[];

  textToSearch: string = '';

  constructor(private translate: TranslateService) { }

  ngOnInit(): void {
  }

  emitFilterItems(): void {
    if (!Boolean(this.textToSearch)) {
      alert(this.translate.instant('err.needSearchText'));
    } else {
      this.filteredItemsE.emit(this.applyFilter(this.textToSearch));
    }
  }

  applyFilter(key: string): ItemI[] {
    if (!this.atrToSearchBy) {
      return this.searchByAllAtr(key);
    } else {
      return this.searchByOwnAtr(key);
    }
  }

  searchByAllAtr(key: string): ItemI[] {
    return this.initialCleanItems.filter(
      item => Object.values(item).some(value => { 
        if (typeof value === 'string')  {
          return value.toLowerCase().includes(key.toLowerCase())
        }
      })
    );
  }
  
  searchByOwnAtr(key: string): ItemI[] {
    return this.initialCleanItems.filter(
      item =>
        this.atrToSearchBy.some(sb => {
          // Para evitar que pete si la key otorgada no existe.
          if (item[sb]) {
            // Si el objeto incluye el valor del atributo proporcionado...
            return item[sb].toLowerCase().includes(key.toLowerCase())
          }
        })
    );
  }

  cleanInput(): void {
    this.textToSearch = '';
    this.filteredItemsE.emit(this.initialCleanItems || []);
  }

}
