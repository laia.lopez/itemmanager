import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { SortbyComponent } from './sortby.component';
import { TranslateService, TranslateStore, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpLoaderFactory } from 'src/app/app.module';
import { GlobalService } from 'src/app/services/global/global.service';
import { By } from '@angular/platform-browser';
import { ItemI } from 'src/app/models/item-interface';
import { OwnTranslateService } from 'src/app/services/own-translate/own-translate.service';

describe('SortbyComponent', () => {
  let component: SortbyComponent;
  let fixture: ComponentFixture<SortbyComponent>;
  let translate: TranslateService;
  let ownTranslateService: OwnTranslateService;
  const itemA: ItemI = { title: 'A', description: 'A', price: '2', email: 'A', image: 'A'}
  const itemB: ItemI = { title: 'B', description: 'B', price: '1', email: 'B', image: 'B'}
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortbyComponent ],
      imports: [
        TranslateModule.forRoot(
          {
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            }
          }
        ),
        HttpClientModule
      ],
      providers: [
        TranslateService,
        TranslateStore
      ]
    })
    .compileComponents();
    translate = TestBed.get(TranslateService);
    ownTranslateService = TestBed.get(OwnTranslateService)
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getResetSelector() should call updateResetSelector and selectVC should be ""',
    (inject([GlobalService], (globalService: GlobalService) => {
    const select = fixture.debugElement.query(By.css('select'));
    globalService.updateResetSelector(true);
    spyOn(globalService, 'updateResetSelector');

    component.getResetSelector();
    fixture.detectChanges()

    expect(select.nativeElement.value).toEqual('');
  })));

  it('orderItems() itemsOrderedByTitle should return first item A', () => {
    component.itemsToOrder = [itemB, itemA];

    const itemsOrderedByTitle = component.orderItems('title');
    expect(itemsOrderedByTitle).toEqual([itemA, itemB]);
  });

  it('orderItems() itemsOrderedPrice should return first item B', () => {
    component.itemsToOrder = [itemB, itemA];

    const itemsOrderedPrice= component.orderItems('price');
    expect(itemsOrderedPrice).toEqual([itemB, itemA]);
  });

  it('orderItems() itemsOrderedPrice with same object', () => {
    component.itemsToOrder = [itemA, itemA];

    const itemsOrderedPrice= component.orderItems('title');
    expect(itemsOrderedPrice).toEqual([itemA, itemA]);
  });


  it('translateF() should call ownTranslateService.getTranslate()', () => {
    spyOn(ownTranslateService, 'getTranslate');
    
    component.translateF('home', 'title');
    expect(ownTranslateService.getTranslate).toHaveBeenCalled();
  });

  it('emitItemsOrdered() the selectedKey is available so itemsOrdered will be emit', () => {
    spyOn(component.itemsOrdered, 'emit');

    component.emitItemsOrdered('title');
    expect(component.itemsOrdered.emit).toHaveBeenCalled();
  });

  it('emitItemsOrdered() the selectedKey is NOT available so itemsOrdered wont emit', () => {
    spyOn(component.itemsOrdered, 'emit');

    component.emitItemsOrdered(null);
    expect(component.itemsOrdered.emit).not.toHaveBeenCalled();
  });
});
