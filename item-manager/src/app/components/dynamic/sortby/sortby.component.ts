import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ItemI } from 'src/app/models/item-interface';
import { OwnTranslateService } from 'src/app/services/own-translate/own-translate.service';
import { GlobalService } from 'src/app/services/global/global.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sortby',
  templateUrl: './sortby.component.html',
  styleUrls: ['./sortby.component.scss']
})
export class SortbyComponent implements OnInit, OnDestroy {
  @ViewChild('selectVC') selectVC: ElementRef; 
  @Output() itemsOrdered: EventEmitter<ItemI[]> = new EventEmitter<ItemI[]>();
  @Input() itemsToOrder: ItemI[] = [];
  @Input() keysToOrder: string[] = [];
  subscription: Subscription;
  
  constructor(private translate: TranslateService, private ownTranslateService: OwnTranslateService, private globalService: GlobalService) {
    this.getResetSelector();
   }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getResetSelector(): void {
    this.subscription = this.globalService.doResetSelector$.subscribe(
                          doReset => {
                            if (doReset) {
                              this.selectVC.nativeElement.value = "";
                              this.globalService.updateResetSelector(false);
                            }
                          }
                        )
  }

  emitItemsOrdered(selectedKey: string): void {
    if (selectedKey) {
      this.itemsOrdered.emit(this.orderItems(selectedKey.toLowerCase()));
    }
  }

  orderItems(selectedKey: string): ItemI[] {
    return this.itemsToOrder.sort((a, b) => {
      let firstItem, secondItem;
      // Si es precio lo convertiremos a número para poder ordenarlo.
      if (selectedKey !== 'price') {
        firstItem = a[selectedKey].toLowerCase();
        secondItem = b[selectedKey].toLowerCase();
      } else {
        firstItem = +a[selectedKey];
        secondItem = +b[selectedKey];
      }

      // Ordenación de más pequeño a más grande.
      return firstItem < secondItem ? -1 : firstItem > secondItem ? 1 : 0;
    });
  }

  translateF(text: string, value: string): string {
    return this.ownTranslateService.getTranslate(text, value);
  }
}
