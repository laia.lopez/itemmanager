import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ItemI } from 'src/app/models/item-interface';

interface InterI {
  items: ItemI[]
  doRefresh: boolean
}

@Injectable({
  providedIn: 'root'
})

export class GlobalService {
  private isModalOpened: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public currentIsModal$ = this.isModalOpened.asObservable();

  private items: BehaviorSubject<InterI> = new BehaviorSubject({items:[], doRefresh: true});
  public currentItems$ = this.items.asObservable();

  private doResetSelector: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public doResetSelector$ = this.doResetSelector.asObservable();

  constructor() { }

  public changeModalOpened(newValue: boolean): void {
    this.isModalOpened.next(newValue);
  }

  public updateItems(items: ItemI[], doRefresh = true){
    this.items.next({ items, doRefresh });
  }

  public updateResetSelector(reset: boolean){
    this.doResetSelector.next(reset);
  }
}
