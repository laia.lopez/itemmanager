import { TestBed } from '@angular/core/testing';

import { GlobalService } from './global.service';

describe('GlobalService', () => {
  let service: GlobalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GlobalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('changeModalOpened result should be true', () => {
    let resultP = false;
    service.changeModalOpened(true);
    service.currentIsModal$.subscribe(result => resultP = result);

    expect(resultP).toBeTruthy();
  });

});
