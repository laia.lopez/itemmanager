import { TestBed } from '@angular/core/testing';

import { DataService } from './data.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('OwnTranslateService', () => {
  let service: DataService;
  let http: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers: [
        DataService
      ]
    });

    http = TestBed.get(HttpClient);
    service = TestBed.inject(DataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('call getItems() should call http.get', () => {
    spyOn(http, 'get').and.returnValue(of());
    service.getItems();
    expect(http.get).toHaveBeenCalled();
  });

  it('call getItems() should return a promise', () => {
    spyOn(http, 'get').and.returnValue(of());
    const result = service.getItems();
    expect(result instanceof Promise).toBeTruthy();
  });
});
