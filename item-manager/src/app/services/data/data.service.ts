import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ItemObjectI } from '../../models/item-object-interface';

@Injectable()
export class DataService {
    corsAnywhere = 'https://cors-anywhere.herokuapp.com/';
    url = 'https://frontend-tech-test-data.s3.eu-west-1.amazonaws.com/items.json';

    constructor (private http: HttpClient) {     }

    getItems(): Promise<ItemObjectI> {
        // Lo pasamos a promesa ya que no necesitamos una búsqueda activa
        return this.http.get<ItemObjectI>(this.corsAnywhere + this.url).toPromise();
    }
}