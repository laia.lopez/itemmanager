import { TestBed } from '@angular/core/testing';

import { OwnTranslateService } from './own-translate.service';
import { TranslateService, TranslateStore, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';

describe('OwnTranslateService', () => {
  let service: OwnTranslateService;
  let translate: TranslateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(
          {
            loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
            }
          }
        ),
        HttpClientModule
      ],
      providers: [
        TranslateService,
        TranslateStore
      ]
    });
    translate = TestBed.get(TranslateService);
    service = TestBed.inject(OwnTranslateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('call getTranslate() should call translate.instant', () => {
    spyOn(translate, 'instant');
    service.getTranslate('menu', 'title');
    expect(translate.instant).toHaveBeenCalled();
  });

  it('should return same string', () => {
    const result = service.getTranslate('menu', 'language');
    expect(result).toBe('menulanguage');
  });
});
