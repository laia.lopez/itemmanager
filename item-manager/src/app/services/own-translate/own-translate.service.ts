import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class OwnTranslateService {

  constructor(public translateService: TranslateService) { }

  getTranslate(text: string, value: string): string {
    return this.translateService.instant(text + value);
  }
}
