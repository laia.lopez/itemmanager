import { ItemI } from './item-interface';

export interface ItemObjectI {
    items: ItemI[];
    length: number;
}