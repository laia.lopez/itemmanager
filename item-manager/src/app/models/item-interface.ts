export interface ItemI {
    title: String,
    description: String,
    price: String,
    email: String,
    image: String,
    favourite?: Boolean
}