export interface ItemTranslatedI {
    lang: string;
    translation: string;
}