import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/pages/home/home.component';
import { HeaderComponent } from './components/general/header/header.component';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { PaginatePipe } from './pipes/paginate.pipe';
import { SearchbyComponent } from './components/dynamic/searchby/searchby.component';
import { SortbyComponent } from './components/dynamic/sortby/sortby.component';
import { DynamicModalComponent } from './components/dynamic/dynamic-modal/dynamic-modal.component';
import { FavouritesModalComponent } from './components/modals/favourites-modal/favourites-modal.component';
import { AboutComponent } from './components/pages/about/about.component';
import { FooterComponent } from './components/general/footer/footer.component';
import { NotFoundComponent } from './components/pages/not-found/not-found.component';

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    PaginatePipe,
    SearchbyComponent,
    SortbyComponent,
    DynamicModalComponent,
    FavouritesModalComponent,
    AboutComponent,
    FooterComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    MatPaginatorModule,
    BrowserAnimationsModule,
    FormsModule,
    TranslateModule.forRoot({
      loader:{
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
