# ItemManager

You will find the JSON that has all the items available in this URL: 
https://frontend-tech-test-data.s3.eu-west-1.amazonaws.com/items.json

**Run project**

Before starting, remember that you must install the Angular client:
`npm install - g @angular/cli`

1.  Download the repository.
2.	Run `npm i` in case there are any missing packages to be installed.
3.	Now, you have two options for running the project. 

    If you're going to test the project on Internet Explorer, I recommend you run the following script I made (it imports the necessary configuration):
`npm run buildie`

    If you are testing from another operating system or simply do not want to use the above script you can use:
`ng serve`



**Testing**

If you want to run the tests you can do it with the following command: 
`ng test`

I have also prepared a script so you can run the tests directly with the code coverage, you just have to run it from the project folder:
`npm run testcov`
